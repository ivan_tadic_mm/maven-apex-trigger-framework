public with sharing class Handler_Opportunity_SetDefaults extends TriggerHandlerAbstract {

	List<Opportunity> newOpps;

	public override void init() {
		newOpps = (List<Opportunity>)this.newRecords;
	}

	public override void run(TriggerEvent event) {
		for (Opportunity opp : newOpps) {
			opp.Description = 'From Trigger ;)';
		}
	}

}