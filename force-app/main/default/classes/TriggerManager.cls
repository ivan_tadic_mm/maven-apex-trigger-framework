public with sharing class TriggerManager {

	// Trigger Handlers Metadata
	private static List<Trigger_Handler__mdt> handlersMeta;
	// Disabled all Triggers
	private static Boolean isDisabled = false;
	// Disabled Handlers
	private static Set<String> disabledHandlers = new Set<String>();
	// Disabled Events
	private static Set<TriggerEvent> disabledEvents = new Set<TriggerEvent>();
	// Disabled Objects
	private static Set<SObjectType> disabledObjects = new Set<SObjectType>();

	/**
	 *	Executes all trigger handlers.
	 *
	 *	@author	Ivan Tadic
	 *	@param	objectType - sObjectType for which trigger handlers will be executed.
	 */
	public static void run(sObjectType objectType) {
		// If all is disabled or object is disabled, skip
		if (isDisabled || disabledObjects.contains(objectType)) {
			return;
		}
		// If Event is disabled, skip
		TriggerEvent event = getCurrentTriggerEvent();
		if (disabledEvents.contains(event)) {
			return;
		}
		// Get Handlers Metadata
		if (handlersMeta == null) {
			getHandlersMeta();
		}
		// Init and run handlers
		for (Trigger_Handler__mdt handlerMeta : handlersMeta) {
			// If Handler is disabled, skip
			if (disabledHandlers.contains(handlerMeta.DeveloperName)) {
				continue;
			}
			// If Handler does not support object, skip
			if (handlerMeta.Object_Name__c.toUpperCase() != objectType.getDescribe().getName()) {
				continue;
			}
			// If Handler is not registered to event, skip
			Set<TriggerEvent> handlerEvents = getHandlerEvents(handlerMeta);
			if (!handlerEvents.contains(event)) {
				continue;
			}
			// Init handler and execute
			TriggerHandlerAbstract handler = (TriggerHandlerAbstract)Type.forName(handlerMeta.DeveloperName).newInstance();
			handler.run(event);
		}
	}

	/**
	 *	Gets Handler Metadata
	 *
	 *	@author	Ivan Tadic
	 */
	private static void getHandlersMeta() {
		handlersMeta = [
			SELECT
				DeveloperName,
				Object_Name__c,
				Trigger_Events__c,
				Execution_Order__c
			FROM
				Trigger_Handler__mdt
			WHERE
				Active__c = true
			ORDER BY
				Execution_Order__c
		];
	}

	/**
	 *	Gets Handler Events from Metadata (Text)
	 *
	 *	@author	Ivan Tadic
	 *	@param	handlerMeta - Handler Metadata.
	 */
	private static Set<TriggerEvent> getHandlerEvents(Trigger_Handler__mdt handlerMeta) {
		Set<TriggerEvent> events = new Set<TriggerEvent>();
		List<String> eventNames = handlerMeta.Trigger_Events__c.split(';');
		for (String eventName : eventNames) {
			events.add(nameToTriggerEvent.get(eventName.trim().toUpperCase()));
		}
		return events;
	}
	private static Map<String, TriggerEvent> nameToTriggerEvent {
		get {
			if (nameToTriggerEvent == null) {
				nameToTriggerEvent = new Map<String, TriggerEvent>();
				for (TriggerEvent te : TriggerEvent.values()) {
					nameToTriggerEvent.put(String.valueOf(te).toUpperCase(), te);
				}
			}
			return nameToTriggerEvent;
		}
		private set;
	}

	/**
	 *	Gets trigger event for current context.
	 *
	 *	@author	Ivan Tadic
	 */
	private static TriggerEvent getCurrentTriggerEvent() {
		if (Trigger.isBefore) {
			if (Trigger.isInsert) {
				return TriggerEvent.BEFORE_INSERT;
			}
			if (Trigger.isUpdate) {
				return TriggerEvent.BEFORE_UPDATE;
			}
			if (Trigger.isDelete) {
				return TriggerEvent.BEFORE_DELETE;
			}
			if (Trigger.isUndelete) {
				return TriggerEvent.BEFORE_UNDELETE;
			}
		} else {
			if (Trigger.isInsert) {
				return TriggerEvent.AFTER_INSERT;
			}
			if (Trigger.isUpdate) {
				return TriggerEvent.AFTER_UPDATE;
			}
			if (Trigger.isUndelete) {
				return TriggerEvent.AFTER_UNDELETE;
			}
		}
		return null;
	}

	/**
	 *	Disables triggers.
	 *
	 *	@author	Ivan Tadic
	 */
	public static void disableTriggers() {
		isDisabled = true;
	}

	/**
	 *	Enables triggers.
	 *
	 *	@author	Ivan Tadic
	 */
	public static void enableTriggers() {
		isDisabled = false;
	}

	/**
	 *	Disables triggers for specific object.
	 *
	 *	@author	Ivan Tadic
	 *	@param	objectType - Object for which triggers will be disabled.
	 */
	public static void disableObject(SObjectType objectType) {
		disabledObjects.add(objectType);
	}

	/**
	 *	Enables triggers for specific object.
	 *
	 *	@author	Ivan Tadic
	 *	@param	objectType - Object for which triggers will be enabled.
	 */
	public static void enableObject(SObjectType objectType) {
		disabledObjects.remove(objectType);
	}

	/**
	 *	Enables triggers for all objects.
	 *
	 *	@author	Ivan Tadic
	 */
	public static void enableAllObjects() {
		disabledObjects = new Set<SObjectType>();
	}

	/**
	 *	Disables trigger event.
	 *
	 *	@author	Ivan Tadic
	 *	@param	event - Trigger event that will be disabled.
	 */
	public static void disableTriggerEvent(TriggerEvent event) {
		disabledEvents.add(event);
	}

	/**
	 *	Enables trigger event.
	 *
	 *	@author	Ivan Tadic
	 *	@param	event - Trigger event that will be enabled.
	 */
	public static void enableTriggerEvent(TriggerEvent event) {
		disabledEvents.remove(event);
	}

	/**
	 *	Enables all trigger events.
	 *
	 *	@author	Ivan Tadic
	 */
	public static void enableAllTriggerEvents() {
		disabledEvents = new Set<TriggerEvent>();
	}

	/**
	 *	Disables specific handler.
	 *
	 *	@author	Ivan Tadic
	 *	@param	handlerName - Handler to be disabled.
	 */
	public static void disableHandler(String handlerName) {
		disabledHandlers.add(handlerName);
	}

	/**
	 *	Enables specific handler.
	 *
	 *	@author	Ivan Tadic
	 *	@param	handlerName - Handler to be enabled.
	 */
	public static void enableHandler(String handlerName) {
		disabledHandlers.remove(handlerName);
	}

	/**
	 *	Enables all handlers.
	 *
	 *	@author	Ivan Tadic
	 */
	public static void enableAllHandlers() {
		disabledHandlers = new Set<String>();
	}

}