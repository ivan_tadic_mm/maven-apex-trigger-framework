public with sharing abstract class TriggerHandlerAbstract {

	// Trigger Records
	protected List<SObject> newRecords;
	protected List<SObject> oldRecords;
	protected Map<Id, SObject> newRecordsMap;
	protected Map<Id, SObject> oldRecordsMap;

	/**
	 *	Constructor.
	 *
	 *	@author	Ivan Tadic
	 */
	public TriggerHandlerAbstract() {
		this.newRecords = Trigger.new;
		this.oldRecords = Trigger.old;
		this.newRecordsMap = Trigger.newMap;
		this.oldRecordsMap = Trigger.oldMap;
		this.init();
	}

	/**
	 *	Init method, called from constructor.
	 *
	 *	@author	Ivan Tadic
	 */
	public abstract void init();

	/**
	 *	Run method. This method contains logic for trigger handler.
	 *
	 *	@author	Ivan Tadic
	 */
	public abstract void run(TriggerEvent event);

	/**
	 *	Checks if record fields have been changed.
	 *	Used in triggers to verify if field is changed as a part of DML.
	 *
	 *	@author	Ivan Tadic
	 *	@param	record - SObject record that should be checked.
	 *	@param	fields - List of field names that should be checked for changes.
	 *	@param	allOrOne - Defines if all fields need to be changed (true) or only one (false).
	 */
	protected Boolean isRecordFieldChanged(SObject record, List<String> fields, Boolean allOrOne) {
		Boolean isChanged = allOrOne;
		if (oldRecordsMap == null) {
			isChanged = true;
		} else {
			sObject oldRecord = oldRecordsMap.get(record.Id);
			for (String field : fields) {
				Boolean isFieldChanged = record.get(field) != oldRecord.get(field);
				if (allOrOne) {
					isChanged = isChanged && isFieldChanged;
				} else {
					isChanged = isChanged || isFieldChanged;
				}
			}
		}
		return isChanged;
	}

	/**
	 *	Checks if record field has been changed.
	 *	Used in triggers to verify if field is changed as a part of DML.
	 *
	 *	@author	Ivan Tadic
	 *	@param	record - SObject record that should be checked.
	 *	@param	field - Field name that should be checked for changes.
	 */
	protected Boolean isRecordFieldChanged(SObject record, String field) {
		return isRecordFieldChanged(record, new List<String>{field}, true);
	}

	/**
	 *	Filters records that have been changed as a part of the DML.
	 *
	 *	@author	Ivan Tadic
	 *	@param	records - SObject records that should be checked.
	 *	@param	fields - List of field names that should be checked for changes.
	 *	@param	allOrOne - Defines if all fields need to be changed (true) or only one (false).
	 */
	protected List<SObject> filterChangedRecords(List<SObject> records, List<String> fields, Boolean allOrOne) {
		List<SObject> result = new List<SObject>();
		for (SObject record : records) {
			if (isRecordFieldChanged(record, fields, allOrOne)) {
				result.add(record);
			}
		}
		return result;
	}

	/**
	 *	Filters records that have been changed as a part of the DML.
	 *
	 *	@author	Ivan Tadic
	 *	@param	records - SObject records that should be checked.
	 *	@param	oldRecords - Map of SObjects before update. null should be passed in a case of insert.
	 *	@param	field - Field name that should be checked for changes.
	 */
	protected List<SObject> filterChangedRecords(List<SObject> records, String field) {
		return filterChangedRecords(records, new List<String>{field}, true);
	}

}