# Apex Trigger Framework

New Salesforce Apex Trigger Framework that supports:

*  Separation of concerns - One handler for each functionality
*  Trigger handler management via custom metadata - Manage your handlers, events, order of execution or deactivate your handlers via custom metadata
*  Trigger engine, event or handler execution control

## Getting Started

After importing classes and custom metadata type, start by invoking trigger engine from your trigger. This will then execute all handlers registered for your object (explained below):

```java
trigger OpportunityTrigger on Opportunity (before insert, before update, after insert, after update) {

    TriggerManager.run(Opportunity.sObjectType);

}
```

Next, we will create new handler that will execute before insert and before update and it will set value of `Opportunity.Description` field:

```java
public with sharing class Handler_Opportunity_SetDefaults extends TriggerHandlerAbstract {

	List<Opportunity> newOpps;

	public override void init() {
		newOpps = (List<Opportunity>)this.newRecords;
	}

	public override void run(TriggerEvent event) {
		for (Opportunity opp : newOpps) {
			opp.Description = 'From Trigger ;)';
		}
	}

}
```

Now, we need to register this handler, so it executed on before insert and before update. Go to *Salesforce > Custom Metadata Types > Trigger Handler* and register new Trigger Handler:

*  Trigger Handler Name: **Handler_Opportunity_SetDefaults** (name of your handler class)
*  Object Name: **Opportunity** (API name of your object)
*  Trigger Events: **BEFORE_INSERT;BEFORE_UPDATE** (list of trigger events on which handler will be executed, semi-colon separated)
*  Execution Order: **1** (execution order of the handler)
*  Active: **true** (activates/deactivates handler)

Repeat last two steps to register additional handlers. Repeat all steps if you need to enable trigger engine for additional object.

## Trigger engine execution control
`TriggerManager` class provides different methods which allow you to control execution of trigger engine. Using this methods you are able to deactivate or activate entire engine, all handlers for a particular object, all handlers subscribed to a trigger event or exactly certain handler. This could be really useful, especially when multiple DMLs are triggered on the same object or when DML is executed on a large set of data. This could cause repeated execution of same triggers, which is not alwayse needed.

* `TriggerManager.disableTriggers()` - Disables entire trigger engine
* `TriggerManager.enableTriggers()` - Enables entire trigger engine
* `TriggerManager.disableObject(SObjectType objectType)` - Disables execution of handlers for given object
* `TriggerManager.enableObject(SObjectType objectType)` - Enables execution of handlers for given object
* `TriggerManager.enableAllObjects()` - Enables execution of handlers for all objects
* `TriggerManager.disableTriggerEvent(TriggerEvent event)` - Disables execution of the given `TriggerEvent`
* `TriggerManager.enableTriggerEvent(TriggerEvent event)` - Enables execution of the given `TriggerEvent`
* `TriggerManager.enableAllTriggerEvents()` - Enables execution of all trigger events
* `TriggerManager.disableHandler(String handlerName)` - Disables execution of the given handler
* `TriggerManager.enableHandler(String handlerName)` - Enables execution of the given handler
* `TriggerManager.enableAllHandlers()` - Enables execution of all handlers

## Trigger handler helper methods
When writing trigger handlers, most common scenario is to check if certain field(s) has changed the value and then execute your logic depending on the outcome. For this purpose, few helper methods have been included in the `TriggerHandlerAbstract` class and therefore are available in all trigger handlers:

* `isRecordFieldChanged(SObject record, List<String> fields, Boolean allOrOne)`
* `isRecordFieldChanged(SObject record, String field)`
* `filterChangedRecords(List<SObject> records, List<String> fields, Boolean allOrOne)`
* `filterChangedRecords(List<SObject> records, String field)`
